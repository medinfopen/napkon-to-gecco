################################################################################
##                         GECCO FROM POP                                     ##
################################################################################
##
## Script name: gecco_from_pop.R
##
## Purpose of script: Create gecco Dataset from POP Secutrial export data.
##
## Author: Khalid Yusuf <olusolakhalid.yusuf@med.uni-goettingen.de>
##
## Date Created: 2022-03-16
##
## -----------------------------------------------------------------------------

# Export Options: 
# delimeter: ";"
# decimal mark: ","
# encoding: utf8

library(tidyverse)


# define directory containing secutrial export files
DATADIR <- file.path("C:/Users/yusuf1/Desktop/Gecco_Project",
                     "POP",
                     "p_export_CSV_NUM3_20211207-102652")


# list all csv files in data directory
datafiles <- list.files(DATADIR, pattern="*.csv")

df_list <- file.path(DATADIR, datafiles) %>%
  map(read_csv2) %>%
  set_names(str_remove(datafiles, ".csv"))

list2env(df_list, envir = .GlobalEnv)

#==============================================================================#
# SECTION: Anamnese/Risikofaktoren - 1.13                                      #
#==============================================================================#

# list of relevant items
pneumo_diseases <- c("gec_pd", "gec_pd_asthma", "gec_pd_copd", "gec_pd_fibr", 
                   "gec_pd_hypert", "gec_pd_ohs", "gec_pd_apn", "gec_pd_osas", 
                   "gec_pd_cf", "gec_pd_oth")

kardio_diseases <- c("gec_cv", "gec_cv_hypert", "gec_cv_mi", "gec_cv_arrhyt", "gec_cv_arrhyt_1", 
                    "gec_cv_pavk", "gec_revasc", "gec_cv_chd", "gec_cv_carot",
                    "gec_cv_oth", "gec_cv_chf", "gec_cv_afib", "gec_cv_chd_nyha")

hepato_diseases <- c("gec_lv", "gec_lv_flc", "gec_lv_cirrh", "gec_lv_inf", 
                    "gec_lv_auto", "gec_lv_oth")

rheuma_diseases <- c("gec_rh", "gec_ced", "gec_rh_ra", "gec_rh_coll", "gec_rh_vasc", 
                     "gec_immu", "gec_rh_other")

diabetes_diseases <- c("gec_dm", "gec_dm_123")

# diabetes set
diab_set <- erstbefragung %>% 
  select(mnppid, all_of(diabetes_diseases)) %>%
  separate(gec_dm_123,
           into = c("Diabetes_Typ", "Diabetes_Group", "Insulin_Behandlung"),
           sep = "\\\\") %>%
  mutate(Diabetes_Typ = gsub("\\(.*", "", Diabetes_Typ, perl=TRUE),
         Insulin_Behandlung = gsub("\\).*", "", Insulin_Behandlung, perl=TRUE),
         Insulin_Behandlung = gsub(", ", "", Insulin_Behandlung, perl=TRUE))


organ_transplant <- c("gec_tx", "gec_tx_heart", "gec_tx_kidney", "gec_tx_lung",
                      "gec_tx_lv", "gec_tx_oth", "gec_tx_oth_org")

neuro_diseases <- c("gec_ne", "gec_ne_park", "gec_ne_dem", "gec_ne_ms", "gec_ne_musc", 
                        "gec_ne_musc_1", "gec_ne_epl", "gec_ne_migr", "gec_ne_ci_hem", 
                        "gec_ne_ci_isch", "gec_ne_psyc", "gec_ne_depr", "gec_ne_anx")

## relevant items for main travel element
general_travel <- erstbefragung %>%
  select(mnppid, gec_trav____1, gec_trav___1, gec_trav___2, gec_trav___3) %>%
  mutate(gec_trav___3 = ifelse(gec_trav___3 == "ja", "nein", gec_trav___3))

# relevant items for foreign travel history
foreign_travel <- erstbefragung %>%
  select(mnppid, gec_trav_country___1, gec_trav_country___2, gec_trav_country___3,
         gec_trav_country___4, gec_trav_country___5, gec_trav_country___6,
         gec_trav_country___7, gec_trav_country___8, gec_trav_country___9)

# relevant items for local travel history
local_travel <- erstbefragung %>%
  select(mnppid, gec_trav_bundesl___1, gec_trav_bundesl___2, gec_trav_bundesl___3,
         gec_trav_bundesl___4, gec_trav_bundesl___5, gec_trav_bundesl___6,
         gec_trav_bundesl___7, gec_trav_bundesl___8, gec_trav_bundesl___9)

# reduce travel set
reduce_travel <- function(df) {
  pickorder <- c("ja", "nein", NA)  
  # reduce each case of travel history to one element 
  trav_set <- df %>%
        mutate(travel_hist = apply(.[-1], 1, function(x) first(x, order_by = match(x,pickorder)))) %>%
        select(mnppid, travel_hist)
}

travel_history <- reduce_travel(general_travel)
travel_country <- reduce_travel(foreign_travel)
travel_bundesland <- reduce_travel(local_travel)

vac_history <- c("vac_sars", "gec_vac_sars_date")

# erstbefragung
other_vac <- c("vac_pneu", "vac_bcg", "vac_infl", "VAC_OTH___1", "VAC_OTH___10",
               "VAC_OTH___2", "VAC_OTH___3", "VAC_OTH___4", "VAC_OTH___5", "VAC_OTH___6",
               "VAC_OTH___7", "VAC_OTH___8", "VAC_OTH___9", "VAC_OTH___UNK")



# collate only cleaned anamnese items
pop_anamnese <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(pneumo %>% select(mnppid, all_of(pneumo_diseases)), by = "mnppid") %>%
  rename(
    Chron_Lungenkrankheit = gec_pd,
    Asthma = gec_pd_asthma,
    COPD = gec_pd_copd,
    Lungenfibrose = gec_pd_fibr,
    Pulmonale_Hypertonie = gec_pd_hypert,
    OHS = gec_pd_ohs,
    Schlafapnoe = gec_pd_apn,
    OSAS = gec_pd_osas,
    Cystische_Fibrose = gec_pd_cf,
    Andere_Chron_Lungenkrankheit = gec_pd_oth) %>%
  left_join(kardio %>% select(mnppid, any_of(kardio_diseases)), by = "mnppid") %>%
  rename(
    Herz_Kreislauf_Erkrankung = gec_cv,
    Bluthochdruck = gec_cv_hypert,
    Zustand_nach_Herzinfarkt = gec_cv_mi,
    Herzrhythmusstrungen = gec_cv_arrhyt,
    Herzrhythmusstrungen_Typ = gec_cv_arrhyt_1,
    Chron_Herzinsuffizienz = gec_cv_chf,
    pAVK = gec_cv_pavk,
    Vorhofflimmern = gec_cv_afib,
    Zustand_nach_Revaskularisation = gec_revasc,
    Koronare_Herzerkrankung = gec_cv_chd,
    NYHA_Status = gec_cv_chd_nyha,
    Carotisstenose = gec_cv_carot,
    Andere_HerzKreislauf_Erkrankung = gec_cv_oth) %>%
  left_join(hepato %>% select(mnppid, any_of(hepato_diseases)), by = "mnppid") %>%
  rename(Chronische_Lebererkrankung = gec_lv,
         Fettleber = gec_lv_flc,
         Leberzirrhose = gec_lv_cirrh,
         Chron_Infektise_Hepatitis = gec_lv_inf,
         Autoimmune_Lebererkrankungen = gec_lv_auto,
         Andere_Lebererkrankung = gec_lv_oth) %>%
  left_join(interview_aufn %>% select(mnppid, any_of(rheuma_diseases)), by = "mnppid") %>%
  rename(Rheumat_Immun_Erkrankung = gec_rh,
         Chronisch_entzndl_Darmerkrankung = gec_ced,
         Rheumatoide_Arthritis = gec_rh_ra,
         Kollagonesen = gec_rh_coll,
         Vaskulitiden = gec_rh_vasc,
         angeborene_Immundefekte = gec_immu,
         Andere_Rheumat_Immun_Erkrankung = gec_rh_other) %>%
  left_join(interview_aufn %>% select(mnppid, gec_hiv), by = "mnppid") %>%
  rename(HIV_Infektion = gec_hiv) %>%
  left_join(interview_aufn %>% select(mnppid, any_of(organ_transplant)), by = "mnppid") %>%
  rename(Organ_oder_Gewebetransplantiert = gec_tx,
         Herz = gec_tx_heart,
         Leber = gec_tx_lv,
         Nieren = gec_tx_kidney,
         Lunge = gec_tx_lung,
         Anderes_Organ_Gewebe = gec_tx_oth,
         Bez_Anderes_Organ_Gewebe = gec_tx_oth_org) %>%
  left_join(diab_set, by = "mnppid") %>%
  rename(Diabetes_Mellitus = gec_dm) %>%
  left_join(interview_aufn %>% select(mnppid, gec_on_solid, gec_on_hema), by = "mnppid") %>%
  left_join(interview_aufn %>% select(mnppid, gec_on_solid_re, gec_on_hema_re), by = "mnppid") %>%
  rename(Solide_Tumorerkrankung = gec_on_solid,
         Hmatoonkologische_Erkrankung = gec_on_hema,
         Remissionsstatus_Solide = gec_on_solid_re,
         Remissionsstatus_Hema = gec_on_hema_re) %>%
  left_join(erstbefragung %>% select(mnppid, gec_smoking), by = "mnppid") %>%
  rename(Smoking = gec_smoking) %>%
  left_join(neuro %>% select(mnppid, any_of(neuro_diseases)), by = "mnppid") %>%
  rename(Chronische_Neurologische = gec_ne,
         Migrne = gec_ne_migr,
         Psychose = gec_ne_psyc,
         Depression = gec_ne_depr,
         Angsterkrankung = gec_ne_anx,
         Epilepsie = gec_ne_epl,
         Multiple_Sklerose = gec_ne_ms,
         M_Parkinson = gec_ne_park,
         Demenz = gec_ne_dem,
         Andere_neuromuskulre_Erkrankungen = gec_ne_musc,
         Andere_neuromuskulre_Erkrankungen_Typ = gec_ne_musc_1,
         Hmorrhagischer_Schlaganfall = gec_ne_ci_hem,
         Ischmischer_Schlaganfall = gec_ne_ci_isch) %>%
  left_join(arztanamnese %>% select(mnppid, oxy), by = "mnppid") %>%
  rename(Oxygenierung = oxy) %>%
  left_join(interview_aufn %>% select(mnppid, gec_ckd, gec_ckd_kdigo_g), by = "mnppid") %>%
  rename(Chronische_Nierenerkrankung = gec_ckd,
         GFR_Klassifikation_nach_KDIGO = gec_ckd_kdigo_g) %>%
  left_join(travel_history %>% select(mnppid, travel_hist), by = "mnppid") %>%
  rename(Positive_Reiseanamnese = travel_hist) %>%
  left_join(travel_bundesland %>% select(mnppid, travel_hist), by = "mnppid") %>%
  rename(Bereistes_Bundesland = travel_hist) %>%
  left_join(travel_country %>% select(mnppid, travel_hist), by = "mnppid") %>%
  rename(Bereistes_Ausland = travel_hist) %>%
  left_join(travel_set %>% select(mnppid, gec_trav_country1), by = "mnppid") %>%
  rename(Andere_Land = gec_trav_country1) %>%
  left_join(erstbefragung %>% select(mnppid, gec_ulc), by = "mnppid") %>%
  rename(Magengeschwre = gec_ulc) %>%
  left_join(interview_aufn %>% select(mnppid, any_of(vac_history)), by = "mnppid") %>%
  left_join(erstbefragung %>% select(mnppid, any_of(other_vac)), by = "mnppid") %>%
  rename(Influenza_Impf = vac_infl,
         Pneumokokken_Impf = vac_pneu,
         BCG_Impf = vac_bcg,
         Corona_Impf = vac_sars,
         Corona_Impf_Date = gec_vac_sars_date)

#==============================================================================#
# SECTION: Bildgebung - 2.13                                      #
#==============================================================================#

# collate only cleaned Bildgebung items
bild_set <- c("gec_ct", "gec_ct_covid19", "gec_xray", 
            "gec_xray_covid19", "gec_lus", "gec_lus_covid19")
 
pop_bildgebung <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(pneumo %>% select(mnppid, all_of(bild_set)), by = "mnppid") %>%
  rename(CT = gec_ct,
         Rntgen = gec_xray,
         US = gec_lus,
         CT_Covid_typischer_Befund = gec_ct_covid19,
         Rntgen_Covid_typischer_Befund = gec_xray_covid19,
         US_Covid_typischer_Befund = gec_lus_covid19)
  
#==============================================================================#
# SECTION: Demographie - 3.13                                      #
#==============================================================================#

# collate only cleaned demographie items
pop_demographie <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(interview_aufn %>% select(mnppid, gec_preg, gec_ethnicity), by = "mnppid") %>%
  left_join(erstbefragung %>% select(mnppid, gec_gender, gec_birthdate, gec_demo_age), by = "mnppid") %>%
  left_join(anthropo %>% select(mnppid, gec_weight, gec_height), by = "mnppid") %>%
  mutate(gec_birthdate = as.Date(as.character(gec_birthdate), "%Y%m%d")) %>%
  left_join(arztanamnese %>% select(mnppid, gec_disab_cfs_uk), by = "mnppid") %>%
  rename(Schwangerschaft = gec_preg,
         Ethnische_Zugehrigkeit = gec_ethnicity,
         Biologisches_Geschlecht = gec_gender,
         Geburtsdatum = gec_birthdate,
         Alter = gec_demo_age,
         Clinical_Frailty_Score = gec_disab_cfs_uk,
         Krpergre_in_cm = gec_height,
         Gewicht_in_Kg = gec_weight)
  
#==============================================================================#
# SECTION: Epidemiologische Faktoren - 4.13                                      #
#==============================================================================#

# collate only cleaned items for Epidemiologische Faktoren
pop_epidemiologische <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(erstbefragung %>% select(mnppid, gec_contact_a, gec_contact_b), by = "mnppid") %>%
  rename(Kontakt_mit_Covid19_erkrankter_Person = gec_contact_a,
         Kontakt_mit_Covid19_verdachter_Person = gec_contact_b)
  

#==============================================================================#
# SECTION: Komplikationen - 5.13                                      #
#==============================================================================#

# collate only cleaned items for Komplikation
kompl_set <- c("gec_coag_dvt", "gec_coag_pe", "gec_coag_thromb_oth",
               "gec_coag_thromb_oth1", "gec_komp_thromb1",
               "gec_komp_pulm", "gec_komp_blut", "gec_komp_thromb2")

pop_komplikation <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(interview_aufn %>% select(mnppid, all_of(kompl_set)), by = "mnppid") %>%
  rename(Vense_Thrombose = gec_coag_dvt,
         Lungenarterienembolie = gec_coag_pe,
         Stroke = gec_komp_thromb1,
         Myokardinfarkt = gec_komp_thromb2,
         Andere_Thrombosis = gec_coag_thromb_oth,
         Andere_Thrombosis_Bezeichnung = gec_coag_thromb_oth1,
         Pulmonale_CoInfektionen = gec_komp_pulm,
         Blutstrominfektionen = gec_komp_blut)
  

#==============================================================================#
# SECTION: Krankheitsbeginn - 6.13                                      #
#==============================================================================#

# collate only cleaned items for Krankheitsbeginn / Aufnahme
pop_aufnahme <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(arztanamnese %>% select(mnppid, gec_diag_severity), by = "mnppid") %>%
  rename(Erkrankungsstadium = gec_diag_severity)


#==============================================================================#
# SECTION: Laborwerte - 7.13                                      #
#==============================================================================#

# collate only cleaned items for Laborwerte Gamma-GT
lab_set <- c("gec_crp", "gec_crp1",	"gec_crp2",
             "gec_ferritin",	"gec_ferritin1",	"gec_ferritin2",
             "gec_bilitotal", "gec_bilitotal1", "gec_bilitotal2",
             "gec_coag_ddimer",	"gec_coag_ddimer1",	"gec_coag_ddimer2",
             "gec_ygt",	"gec_ygt1",	"gec_ygt2", "gec_ast",	"gec_ast2",
             "gec_ldh",	"gec_ldh2", "gec_tropt",	"gec_tropt1",	"gec_tropt2",
             "gec_bc_hb",	"gec_bc_hb2", "gec_crea", "gec_crea2", "gec_bc_wbc",
             "gec_bc_wbc2", "gec_bc_lymphoc",	"gec_bc_lymphoc_per",	"gec_bc_lymphoc_per2",	
             "gec_bc_lymphoc2", "gec_bc_neutroph",	"gec_bc_neutroph_per", "gec_bc_neutroph_per2",
             "gec_bc_neutroph2", "gec_coag_aptt", "gec_coag_aptt2",
             "gec_bc_platelet", "gec_bc_platelet2", "gec_albumin",
             "gec_albumin2", "gec_coag_at3",	"gec_coag_at31",	"gec_coag_at32",
             "gec_pct", "gec_pct2", "gec_il6",	"gec_il61",	"gec_il62",
             "gec_ntprobnp",	"gec_ntprobnp1",	"gec_ntprobnp2", "gec_coag_fibrinog",
             "gec_coag_fibrinog2", "gec_coag_inr",	"gec_coag_inr2", "gec_pcr_resultql",	
             "gec_pcr_resultql2", "gec_iga_resultql",	"gec_iga_resultql1",	"gec_iga_resultql2",
             "gec_igg_resultqn",	"gec_igg_resultqn1",	"gec_igg_resultqn2")

pop_labwerte <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(laborwerte %>% select(mnppid, all_of(lab_set)), by = "mnppid") %>%
  rename(CRP = gec_crp,
         CRP_Zusatzinformation = gec_crp1,
         CRP_Material = gec_crp2,
         Ferritin = gec_ferritin,
         Ferritin_Zusatzinformation = gec_ferritin1,
         Ferritin_Material = gec_ferritin2,
         Bilirubin = gec_bilitotal,
         Bilirubin_Zusatzinformation = gec_bilitotal1,
         Bilirubin_Material = gec_bilitotal2,
         D_dimer = gec_coag_ddimer,
         D_dimer_Zusatzinformation = gec_coag_ddimer1,
         D_dimer_Material = gec_coag_ddimer2,
         Gamma_GT = gec_ygt,
         Gamma_GT_Zusatzinformation = gec_ygt1,
         Gamma_GT_Material = gec_ygt2,
         AST = gec_ast,
         AST_Material = gec_ast2,
         LDH = gec_ldh,
         LDH_Material = gec_ldh2,
         Kardiale_Troponine = gec_tropt,
         Kardiale_Troponine_Zusatzinformation = gec_tropt1,
         Kardiale_Troponine_Material = gec_tropt2,
         Hmoglobin = gec_bc_hb,
         Hmoglobin_Material = gec_bc_hb2,
         Kreatinin = gec_crea,
         Kreatinin_Material = gec_crea2, 
         Leukozyten_absolut = gec_bc_wbc,
         Leukozyten_absolut_Material = gec_bc_wbc2,
         Lymphozyten_absolut = gec_bc_lymphoc,
         Lymphozyten_absolut_Material = gec_bc_lymphoc2,
         Lymphozyten_absolut_PER = gec_bc_lymphoc_per,
         Lymphozyten_absolut_PER_Material = gec_bc_lymphoc_per2,
         Neutrophile_absolut = gec_bc_neutroph,
         Neutrophile_absolut_Material = gec_bc_neutroph2,
         Neutrophile_absolut_PER = gec_bc_neutroph_per,
         Neutrophile_absolut_PER_Material = gec_bc_neutroph_per2,
         PTT = gec_coag_aptt,
         PTT_Material = gec_coag_aptt2,
         Thrombozyten_absolut = gec_bc_platelet,
         Thrombozyten_absolut_Material = gec_bc_platelet2,
         Serum_Albumin = gec_albumin,
         Serum_Albumin_Material = gec_albumin2,
         Antithrombin_III = gec_coag_at3,
         Antithrombin_III_Zusatzinformation = gec_coag_at31,
         Antithrombin_III_Material = gec_coag_at32,
         PCT = gec_pct,
         PCT_Material = gec_pct2,
         Il_6 = gec_il6,
         Il_6_Zusatzinformation = gec_il61,
         Il_6_Material = gec_il62,
         NT_pro_BNP = gec_ntprobnp,
         NT_pro_BNP_Zusatzinformation = gec_ntprobnp1,
         NT_pro_BNP_Material = gec_ntprobnp2,
         Fibrinogen = gec_coag_fibrinog,
         Fibrinogen_Material = gec_coag_fibrinog2,
         INR = gec_coag_inr,
         INR_Material = gec_coag_inr2,
         SARS_CoV2_RT_PCR = gec_pcr_resultql,
         SARS_CoV2_RT_PCR_Material = gec_pcr_resultql2,
         SARS_CoV2_IgA = gec_iga_resultql,
         SARS_CoV2_IgA_Zusatzinformation = gec_iga_resultql1,
         SARS_CoV2_IgA_Material = gec_iga_resultql2,
         SARS_CoV2_IgG = gec_igg_resultqn,
         SARS_CoV2_IgG_Zusatzinformation = gec_igg_resultqn1,
         SARS_CoV2_IgG_Material = gec_igg_resultqn2)

#==============================================================================#
# SECTION: Medikation - 8.13                                      #
#==============================================================================#

# collate only cleaned items for Medikation
covidmed <- c("gec_med_covid1", "gec_med_covid2", "gec_med_covid3",	"gec_med_covid4", "gec_med_covid5",	"gec_med_covid6",	"gec_med_covid7",	
              "gec_med_covid8",	"gec_med_covid9",	"gec_med_covid10",	"gec_med_covid11",	"gec_med_covid12",	"gec_med_covid13",	
              "gec_med_covid14", "gec_med_covid15",	"gec_med_covid16",	"gec_med_covid17",	"gec_med_covid18",	"gec_med_covid19",	
             "gec_med_covid20",	"gec_med_covid21",	"gec_med_covid22",	"gec_med_covid23",	"gec_med_covid24",	"gec_med_covid25",
             "gec_med_covid26",	"gec_med_covid27",	"gec_med_covid28",	"gec_med_covid28_1")


acemed <- c("gec_med_ace", "gec_med_ace_produkt")

immunmed <- c("gec_med_immun", "gec_med_immun_prod")

antimed <- c("gec_med_anti1",	"gec_med_anti2",	"gec_med_anti3",	"gec_med_anti4",	"gec_med_anti5",	"gec_med_anti6",	
                 "gec_med_anti7",	"gec_med_anti8",	"gec_med_anti8_1")

pop_medikation <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(arztanamnese %>% select(mnppid, all_of(covidmed), all_of(acemed), all_of(immunmed), all_of(antimed)), by = "mnppid") %>%
  rename(Antipyretika = gec_med_covid1,
         Kortikosteroide = gec_med_covid2,
         Atazanavir = gec_med_covid3,
         Darunavir = gec_med_covid4,
         Chloroquine_phosphate = gec_med_covid5,
         Hydroxychloroquine = gec_med_covid6,
         Ivermectin = gec_med_covid7,
         Lopinavir = gec_med_covid8,
         Ganciclovir = gec_med_covid9,
         Oseltamivir = gec_med_covid10,
         Remdesivir = gec_med_covid11,
         Ribavirin = gec_med_covid12,
         Camostat = gec_med_covid13,
         Favipiravir = gec_med_covid14,
         Convalescent = gec_med_covid15,
         Steroids_1 = gec_med_covid16,
         Steroids_2 =  gec_med_covid17,
         Tocilizumab = gec_med_covid18,
         Sarilumab = gec_med_covid19,
         CNI_inhibitors  = gec_med_covid20,
         Anti_TNF_alpha_inhibitors = gec_med_covid21,
         Il1_receptor = gec_med_covid22,
         Ruxolitinib = gec_med_covid23,
         Colchicine = gec_med_covid24,
         Interferone = gec_med_covid25,
         HydroxyvitaminD = gec_med_covid26,
         Zinc = gec_med_covid27,
         Andere_Covid_Therapie = gec_med_covid28,
         Andere_Covid_Therapie_Bez. = gec_med_covid28_1,
         Unfraktioniertes_Heparin = gec_med_anti1,
         Niedermolekulares_Heparin = gec_med_anti2,
         Argatroban = gec_med_anti3,
         Plttchenaggregationshemmer = gec_med_anti4,
         Danaparoid = gec_med_anti5,
         Phenprocoumon = gec_med_anti6,
         DOAK = gec_med_anti7,
         Andere_Antikoagulant = gec_med_anti8,
         Andere_Antikoagulant_Bez.  = gec_med_anti8_1,
         ACE_Hemmer = gec_med_ace,
         ACE_Hemmer_Typ = gec_med_ace_produkt,
         Immunglobuline = gec_med_immun,
         Immunglobuline_Typ = gec_med_immun_prod)

#==============================================================================#
# SECTION: Outcome bei Entlassung - 9.13                                      #
#==============================================================================#

# collate only cleaned items for Entlassung 
# no Entlassungsart in pop
pop_entlassung <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(arztanamnese %>% select(mnppid, out_resp), by = "mnppid") %>%
  left_join(laborwerte %>% select(mnppid, gec_pcr_resultql, gec_pcr_resultql2), by = "mnppid") %>%
  rename(Respiratory_Outcome = out_resp,
         SARS_CoV2_RT_PCR = gec_pcr_resultql,
         SARS_CoV2_RT_PCR_Material = gec_pcr_resultql2)

#==============================================================================#
# SECTION: Einschlusskriterien - 10.13                                      #
#==============================================================================#

# collate only cleaned items for Einschlusskriterien
# no covid19 aufnahme and study teilnahme items in pop?
# pop_einschluss <- cn %>%
#   select(mnppid, mnppsd)

#==============================================================================#
# SECTION: Symptome - 11.13                                      #
#==============================================================================#

# collate only cleaned items for symptome
symptom_set <- c("gec_sy",	"gec_sy_diar", "gec_sym_durc_schwer",	"gec_sy_fever",	
                 "gec_sym_fieb_schwer", "gec_sy_gi_abdp", "gec_sym_bau_schwer",	
                 "gec_sy_gi_naus", "gec_sym_uebel_schwer",	"gec_sy_gi_vom",	
                 "gec_sym_erb_schwer", "gec_sy_heada", "gec_sym_kopf_schwer",
                 "gec_sy_ne_conf", "gec_sym_bewu_schwer", "gec_sym_ger_schwer",	"gec_sy_ne_smell", 	
                 "gec_sy_ne_taste", "gec_sy_ne_taste_sch", "gec_sy_pd_cough", 
                 "gec_sym_hust_schwer", "gec_sy_pd_dysp", "gec_sym_kurza_schwer",
                 "gec_symp_oth",	"gec_symp_oth_1",	"gec_symp_oth_schwer")

pop_symptome <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(erstbefragung %>% select(mnppid, all_of(symptom_set)), by = "mnppid") %>%
  rename(Symptome_allgemein = gec_sy,
         Durchfall  = gec_sy_diar,
         Feber = gec_sy_fever,
         Bauchschmerzen = gec_sy_gi_abdp,
         belkeit = gec_sy_gi_naus,
         Erbrechen = gec_sy_gi_vom,
         Kopfschmerzen = gec_sy_heada,
         Bewusstseinsstrungen = gec_sy_ne_conf,
         Geruchstrungen = gec_sy_ne_smell,
         Geschmacksstrungen = gec_sy_ne_taste,
         Geschmacksstrungen_Schwergrad = gec_sy_ne_taste_sch,
         Husten = gec_sy_pd_cough,
         Kurzatmigkeit = gec_sy_pd_dysp,
         Bauchschmerzen_Schwergrad = gec_sym_bau_schwer,
         Bewusstseinsstrungen_Schwergrad  = gec_sym_bewu_schwer,
         Durchfall_Schwergrad  = gec_sym_durc_schwer,
         Erbrechen_Schwergrad  = gec_sym_erb_schwer,
         Feber_Schwergrad  =  gec_sym_fieb_schwer,
         Geruchstrungen_Schwergrad  =  gec_sym_ger_schwer,
         Husten_Schwergrad  =  gec_sym_hust_schwer,
         Kopfschmerzen_Schwergrad  =  gec_sym_kopf_schwer,
         Kurzatmigkeit_Schwergrad  = gec_sym_kurza_schwer,
         belkeit_Schwergrad  =  gec_sym_uebel_schwer,
         Andere_Symptom = gec_symp_oth,
         Andere_Symptom_Bez. = gec_symp_oth_1,
         Andere_Symptom_Schwergrad = gec_symp_oth_schwer)
  
#==============================================================================#
# SECTION: Therapie - 12.13                                      #
#==============================================================================#

# collate only cleaned items for Therapie
therapie_set <- c("gec_ther_aph", "gec_pronepos", "gec_exgas", "intens_aufent", 
                  "oxy", 	"gec_oxy_type", "sauerst_ther_art")

pop_theraphie <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(interview_aufn %>% select(mnppid, gec_komp_dialyse), by = "mnppid") %>%
  left_join(arztanamnese %>% select(mnppid, all_of(therapie_set)), by = "mnppid") %>%
  rename(Dialyse = gec_komp_dialyse,
         Apherese = gec_ther_aph,
         Bauchlage = gec_pronepos,
         ECMO_Therapie = gec_exgas,
         Intensivstation = intens_aufent,
         Beatmungstherapie = oxy,
         Beatmungstherapie_typ = gec_oxy_type,
         Sauerstoff_Therapie_typ = sauerst_ther_art)

#==============================================================================#
# SECTION: Vitalparameter - 13.13                                      #
#==============================================================================#

# collate only cleaned items for Vitalparameter
#' no FiO2 & Sofa
vital_set <- c("gec_vitals_resp", "gec_vitals_pdias", "gec_vitals_psys",  "gec_vitals_temp") #anthropo
pneumo_set <- c("gec_bga_pco2", "gec_bga_po2", "gec_bga_ph", "gec_vitals_so2") #pneumo

pop_vitals <- cn %>%
  select(mnppid, mnppsd) %>%
  left_join(pneumo %>% select(mnppid, all_of(pneumo_set)), by = "mnppid") %>%
  left_join(anthropo %>% select(mnppid, all_of(vital_set)), by = "mnppid") %>%
  left_join(kardio %>% select(mnppid, gec_vitals_hf), by = "mnppid") %>%
  rename(PaCO2 = gec_bga_pco2,
         PCO2 = gec_bga_po2,
         pH_Wert = gec_bga_ph,
         Atemfrequenz = gec_vitals_resp,
         Blutdruck_diastol = gec_vitals_pdias,
         Blutdruck_systol = gec_vitals_psys,
         Krpertemperatur = gec_vitals_temp,
         Herzfrequenz = gec_vitals_hf,
         P_Sauerstoffsttigung = gec_vitals_so2)
  
#  export csv files
EXPORTDIR <- file.path("C:/Users/yusuf1/Desktop/Gecco_Project/POP", "pop_set")

pop_files <- sort(str_subset(names(globalenv()), "^pop_"))
#popu_files <- setdiff(pop_files, c("pop_files"))
pop_files %>%
  walk(~ {write_csv(get(.),
                    str_c(file.path(EXPORTDIR, .), ".csv"),
                    na = "")})  

